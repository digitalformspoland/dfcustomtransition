//
//  DFModalFirstViewController.swift
//  DFCustomTransition
//
//  Created by Mariusz Graczkowski on 16.03.2017.
//  Copyright © 2017 Digitalforms. All rights reserved.
//

import UIKit

class DFModalFirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.transitioningDelegate = self
    }
    
    @IBAction func closeHandler(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
}

extension DFModalFirstViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DFCustomAnimationController();
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = DFCustomAnimationController()
        animationController.reverse = true
        return animationController
    }
}
