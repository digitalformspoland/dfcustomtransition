//
//  DFCustomAnimationController.swift
//  DFCustomTransition
//
//  Created by Mariusz Graczkowski on 16.03.2017.
//  Copyright © 2017 Digitalforms. All rights reserved.
//

import UIKit

class DFCustomAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var reverse: Bool = false
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
        let containerView = transitionContext.containerView
        let bounds = UIScreen.main.bounds
        
        var animationBlock: () -> ()
        var completionBlock: (_ finished: Bool) -> ()
        
        if self.reverse {
            containerView.addSubview(toViewController.view)
            toViewController.view.alpha = 0.0
            
            animationBlock = {
                toViewController.view.alpha = 1.0
            }
            
            completionBlock = { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
            
        } else {
            toViewController.view.frame = finalFrameForVC.offsetBy(dx: 0, dy: -bounds.size.height)
            containerView.addSubview(toViewController.view)
            
            animationBlock = {
                fromViewController.view.alpha = 0.5
                toViewController.view.frame = bounds
            }
            
            completionBlock = { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                fromViewController.view.alpha = 1.0
            }
        }
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: .curveLinear, animations: animationBlock, completion: completionBlock)
    }
}
