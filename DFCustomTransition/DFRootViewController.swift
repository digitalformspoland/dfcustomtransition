//
//  DFRootViewController.swift
//  DFCustomTransition
//
//  Created by Mariusz Graczkowski on 16.03.2017.
//  Copyright © 2017 Digitalforms. All rights reserved.
//

import UIKit

class DFRootViewController: UITableViewController {

    let dataSource: [String] = ["Custom transition VC (Present Modally)", "Custom transition VC (in stock NC)"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath);
        cell.textLabel?.text = dataSource[indexPath.row];
        
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            case 0:
                self.performSegue(withIdentifier: "segueModalVC", sender: nil);
                
            case 1:
                self.performSegue(withIdentifier: "segueVC", sender: nil);
            
            default:
                break
        }
    }
}

extension DFRootViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animationController = DFCustomAnimationController()
        animationController.reverse = operation == .pop
        
        return animationController
    }
}
