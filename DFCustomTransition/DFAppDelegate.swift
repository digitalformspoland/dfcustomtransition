//
//  DFAppDelegate.swift
//  DFCustomTransition
//
//  Created by Mariusz Graczkowski on 16.03.2017.
//  Copyright © 2017 Digitalforms. All rights reserved.
//

import UIKit

@UIApplicationMain
class DFAppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

